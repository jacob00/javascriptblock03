var lowerCaseLetters = (str) =>{
	var newStr = [];
	var str2 = str.split('');
	str2.forEach(function(item, index) {
		if(isNaN(item)) {
			if(item === item.toUpperCase()) {
				newStr.push(' ');
			}
		newStr.push(item.toLowerCase());
		}
	});
return newStr.join('').trim();
}
	
module.exports = {
	lowerCaseLetters
}