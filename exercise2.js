var i = 11;
console.log('Before -->',i)
var firstLoopReverse = () =>{
    for(i; i>=1;i--){
        console.log('In loop -->',i)
    }
    console.log('After -->',i)
    return i; 
}
module.exports = {
    firstLoopReverse, i
}