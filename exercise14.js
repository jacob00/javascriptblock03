var budgetTracker = (arr) =>{
    total = 0
    arr.forEach(function(item){
        
        total += parseInt(item)
    })
    
    return Math.round(total / arr.length * 0.0089)
}

module.exports = {
    budgetTracker
}