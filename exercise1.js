var i=1;

var firstLoop = () =>{ 
      console.log('before the loop -->',i)
      for (i; i<=10; i++) { 
                 console.log('in the loop -->',i); 
      } 
      console.log('after the loop -->',i)
      return i; 
} 

module.exports = {
    firstLoop, i
}