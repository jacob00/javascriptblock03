var reverser = (str) => {
    var str2 = []
    for (var i = str.length-1; i>=0; i--){
        str2.push(str[i])
    }
    return str2.join("");
}

module.exports = {
    reverser
}
