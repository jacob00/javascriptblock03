var isEven = (arr) =>{
    var count = 0
    console.log('Before loop --> ', count)
        for(var i=0; i<arr.length; i++){
        if(arr[i] % 2 == 0){
            count++
            console.log("During loop --> ",count)
        }
    }   
    console.log("After loop --> ", count)
    return count
}


module.exports ={
    isEven
}