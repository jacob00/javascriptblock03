var shortener = (str) => {
    var strSplitted = str.split(' ');
    var name = strSplitted[0]
    var upperName = name.charAt(0).toUpperCase() + name.slice(1)
    var initials = upperName + ' ' + strSplitted[1].charAt(0).toUpperCase() + '.'
    
    return initials;
}

module.exports = {
    shortener
}