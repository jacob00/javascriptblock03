var multy = (arr) => {
    var total = 1
    arr.forEach(function(ele){
        total *= ele
    })
    return total
}

module.exports ={
    multy
}