var longestString = (arr, minLength) => {
    var longestWord =[]

    for(i = arr.length-1; i >= 0; i--){
        if(arr[i].length > minLength){
            longestWord.push(arr[i])
        }
    }
    return longestWord[0]
}
module.exports = {
    longestString
}
