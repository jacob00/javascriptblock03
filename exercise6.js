
var sum = (arr) =>{
    var total = 0
    arr.forEach(function(element){
        total += element
    })
    return total 
}
module.exports ={
    sum
}