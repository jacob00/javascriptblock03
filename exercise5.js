var anyArr = ['one', 5, 'two', 6, 'three', true, 'four']

var isString = (anyArr) => {
    var arr = []
    anyArr.forEach(function(element){
        if (typeof element === 'string'){
            
            arr.push(element)
        }
    })
    return arr;
}

module.exports = {
    isString
}

